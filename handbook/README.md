
## Building the BoK

The following packages are required to build the BoK.

Installation commands examples for debian family distros.

- pandoc (version >=  2.x): `apt install pandoc`
- python3
- python3 pip module: `apt install python3-pip`
- python3 gitlab package: `python3 -m pip install python-gitlab`
