######################################################################
# Copyright (c) 2021 Castalia Solutions and others
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
######################################################################

import gitlab
import pprint
import re

gl_url = "https://gitlab.ow2.org"
gl = gitlab.Gitlab(gl_url, per_page=50)

pp = pprint.PrettyPrinter(indent=4)

project_id = 1654
project = gl.projects.get(project_id)
print("Project is:", project.id)

#issues = project.issues.list()
print("# Fetching issues..")
issues = project.issues.list(state='opened', all=True)

print("# Exporting issues..")
title_re = re.compile("^\((.* Goal)\) (.*)$")

# The prefixes that will be used to create files. 
table_goals = {'Usage Goal': 51,
               'Trust Goal': 52,
               'Culture Goal': 53,
               'Engagement Goal': 54,
               'Strategy Goal': 55}

for i in issues:
    title_m = title_re.search(i.title)
    if title_m is None:
        print(f"* Issue {i.iid} not compliant, skipping.")
        continue
    title = title_m.group(2)
    print(f"* Issue {i.iid} - {title}")
    
    sections = i.description.split("\n## ")

    # Remove first empty section before description.
    sections.pop(0)

    # Manage labels
    k = []
    prefix: str
    for label in i.labels:
        m = label.replace(" ", "_")
        k.append(m)
        if label in table_goals:
            prefix = table_goals[label]
    tags = "[\"" + '","'.join(k) + "\"]"

    # Open output file for writing
    fname = f"content/{prefix}_activity_{str(i.iid)}.md"
    f = open(fname, "w")

    # Write title
    f.write(f"## {title}\n\n")

    # Write tags
#    f.write(f"Tags: {tags}\n\n")

    # Write URL to activity in gitlab
    url = f"https://gitlab.ow2.org/ggi/ggi-castalia/-/issues/{i.iid}"
    f.write(f"Link to GitLab issue: <{url}>.\n\n")
    
    # Write section content
    for section in sections:
        sec_lines = section.split("\n")
        sec_title = sec_lines.pop(0)
        f.write(f"### {sec_title} \n")
        lines = '\n'.join(sec_lines)
        f.write(f"{lines}\n")

    f.close()

