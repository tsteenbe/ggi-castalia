Hi, I'm Boris, manager in charge of open-source development at MyGreatOgranisation.

## My Context

MyGreatOrganisation provides services for the domain of MyGreatDomain. 

Sizing: 30 people, 1000+ computers, geographically distributed.

Open-source context: 

* We have produced some open-source for 2 years, there is some basic open-source knowledge within some of our teams.
* We use a lot of oss.
* No OSPO, but interest and concern about oss.
* Open-source is assumed by the top management but they want to be reassured.

## My pain points

I need to:

* Secure a solid and consistent OSS strategy.
* Guarantee to the hierarchy a best-in-class approach to open-source (good citizenship and governance). 
* Need to communicate about open-source towards the outside (reputation).
* Re-enforce state-of-the-art policies and practices for OS management.
* Make sure that our open-source products are well managed open-source wise.

## How I did it

We started a discussion with the Good Governance Initiative working group.

They helped me prioritize the activities and build a customized and well-structured roadmap.

I went through the list of activities, picking a subset for our first iteration. I was able to discuss pain points with members of the group and collaboratively we could answer my questions.

## Achievements

Ideally a list of secured benefits corresponding to the list of pain points mentioned before. 

* Defined and set up processes to ensure that open-source used within the company is safe. People now know open-source and are reassured regarding legal and technical risks.
* Set up a communication campaign both for management and the public regarding our use and involvement in open-source.
* Defined a sound and consistent strategy for open-source management for the management, with regular updates and progress.
* 